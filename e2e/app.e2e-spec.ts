import { AngularfortestPage } from './app.po';

describe('angularfortest App', function() {
  let page: AngularfortestPage;

  beforeEach(() => {
    page = new AngularfortestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
