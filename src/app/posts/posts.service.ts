import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';

@Injectable()
export class PostsService {
//private _url = 'http://jsonplaceholder.typicode.com/posts';

postsObservabale;

getPosts(){
    this.postsObservabale = this.af.database.list('/posts').map(//טבלה שעוברים עליה
    posts =>{
      posts.map(
        post => {
          post.userName =[];
          for(var u in post.users){//לכל פוסט עוברים על תכונה users
            post.userName.push(//יוצרים מערך של כל היוזרים שכתבו את הפוסט
              this.af.database.object('/users/' + u)
            )
          } }
      );
      return posts;}
      );
  return this.postsObservabale;
  }


  addPost(post){
  this.postsObservabale.push(post);
}
updatePost(post){
  let postKey= post.$key;
  let postData = {body:post.body,title:post.title};
  this.af.database.object('/posts/' + postKey).update(postData);
}

deletePost(post){
let postKey= post.$key;
this.af.database.object('/posts/' + postKey).remove();

}
constructor(private af:AngularFire) { }

}