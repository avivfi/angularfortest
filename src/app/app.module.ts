import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import {UsersService} from './users/users.Service';
import { UserComponent } from './user/user.component';
import { PostsComponent } from './posts/posts.component';
import {PostsService} from './posts/posts.Service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import {RouterModule, Routes} from '@angular/router';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';
import{AngularFireModule} from 'angularfire2';

export const firebaseConfig = {
    apiKey: "AIzaSyClbb3ZNzK406pDy531QLPXDKBx0kEc_Ec",
    authDomain: "angularexemple.firebaseapp.com",
    databaseURL: "https://angularexemple.firebaseio.com",
    storageBucket: "angularexemple.appspot.com",
    messagingSenderId: "228418704515"
} 

const appRoutes:Routes=[
  {path:'users',component:UsersComponent},
  {path:'posts',component:PostsComponent},
  {path:'',component:UsersComponent},
  {path:'**',component:PagenotfoundComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    UserComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PagenotfoundComponent,
    UserFormComponent,
    PostFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)

  ],
  providers: [UsersService,PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
