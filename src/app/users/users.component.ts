import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.Service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #FF9999	; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f8;
         border-color: #ecf0f7; 
         color: #FF9999;
    }     
  `]
})
export class UsersComponent implements OnInit {

users;

currentUser;

islodding:Boolean= true;

select(user){

  this.currentUser = user;
  }
  


 addUser(user){
    this._usersService.addUser(user);
    
  }

 updateUser(user){
    this._usersService.updateUser(user);
    
  }

  deleteUser(user){
  //this.users.splice(this.users.indexOf(user),1)
  this._usersService.deleteUser(user);
  }

  constructor(private _usersService:UsersService) { }

  ngOnInit() {
  this._usersService.getUsers().subscribe(usersData =>{  
  this.users= usersData;
  this.islodding=false;
  console.log(this.users)});
}
}