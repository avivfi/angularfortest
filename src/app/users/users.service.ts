import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';


@Injectable()
export class UsersService {

//private _url = 'http://jsonplaceholder.typicode.com/users';

usersObservabale;


getUsers(){
    this.usersObservabale = this.af.database.list('/users').map(
    users =>{
      users.map(
        user => {
          user.postTitle =[];
          for(var u in user.posts){
            user.postTitle.push(
              this.af.database.object('/posts/' + u)
            )
          } }
      );
      return users;}
      );
  return this.usersObservabale;
  }

addUser(user){
  this.usersObservabale.push(user);
}

updateUser(user){
  let userKey= user.$key;
  let userData = {name:user.name,email:user.email};
  this.af.database.object('/users/' + userKey).update(userData);
}

deleteUser(user){
 let userKey= user.$key;
this.af.database.object('/users/' + userKey).remove();

}

constructor(private af:AngularFire) { }

}
